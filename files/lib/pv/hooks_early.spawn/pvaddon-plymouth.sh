#!/bin/sh

source /etc/plymouth-addon/plymouth.settings
CHROOT_FS=plymouth
CHROOT_IO=$CHROOT_FS/container-io

mkdir -p plymouth_fs plymouth_ovl/upper plymouth_ovl/work plymout_ovl/lower plymouth
mount -t squashfs /usr/local/lib/plymouth/root.squashfs /plymouth_fs
mount -t overlay -o lowerdir=/plymouth_fs,upperdir=/plymouth_ovl/upper,workdir=plymouth_ovl/work none /plymouth
mkdir -p /volumes/$CONTAINER/docker--var-run-plymouth-io-sockets $CHROOT_IO
ls / /volumes/$CONTAINER/docker--var-run-plymouth-io-sockets $CHROOT_FS $CHROOT_IO > /ls.log 2>&1
mount --bind /volumes/$CONTAINER/docker--var-run-plymouth-io-sockets $CHROOT_IO > mount.log 2>&1
/sbin/chroot /plymouth /usr/local/bin/start_plymouth.sh
umount /plymouth/$CHROOT_IO
umount /plymouth
umount /plymouth_fs
rm -rf plymouth_ovl

