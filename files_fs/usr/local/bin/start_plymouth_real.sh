#!/bin/sh

CONTAINER_IO_FS=/container-io
CONTAINER_QUIT_FILE=$CONTAINER_IO_FS/plymouth-quit

log()
{
	echo "[plymouth_script]: $*"
	echo "[plymouth_script]: $*" >> plymouth.log
}

active_wait()
{
	sleep 0.1
}

prepare_plymouth()
{
	/sbin/mdev -s
	/sbin/udevd --daemon
	udevadm trigger

	/sbin/plymouthd --debug --no-daemon --kernel-command-line="splash plymouth.ignore-serial-consoles" --mode=boot &
	pid=$!

	log "waiting for plymouth demon..."
	while ! plymouth --ping ; do
		active_wait
	done
	log "... daemon up!"
}

mkdir -p /dev
mount -t tmpfs none /dev

mkdir -p /sys
mount -t sysfs none /sys

prepare_plymouth
log "showing splash..."
plymouth show-splash &
log "... done"

# fix for rpi4 vc4-fkms-v3d:
# vc4-fkms-v3d breaks plymouth completely. Restart plymouth once the driver is loaded
#log "waiting for kms module ..."
#while [ -z "$(dmesg | grep '\[drm\]')" ]; do
#	active_wait
#done
#
#log "driver loaded, restarting plymouth"
#
#plymouth quit
#prepare_plymouth
#log "showing splash..."
#plymouth show-splash &
#log "... done"
# end fix for rpi4

log "waiting for container io filesystem at $CONTAINER_IO_FS ..."
while [ ! -d $CONTAINER_IO_FS ]; do
	active_wait
done
log "... done"

log "waiting for file $CONTAINER_QUIT_FILE ..."

touch $CONTAINER_IO_FS/plymouth_up

while [ ! -f $CONTAINER_QUIT_FILE ]; do
	active_wait
done
log "... done"

log "exiting plymouth ..."
plymouth quit --retain-splash

# wait for plymouth to actually quit
while [ plymouth --ping ]; do
	active_wait
done
rm $CONTAINER_QUIT_FILE

log "... done"

