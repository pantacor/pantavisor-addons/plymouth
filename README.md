# Pantavisor Boot Splash Plugin

This repository holds the files to create a pantavisor boot splash plugin.

The dockerfile expects three arguments:
 - `CONTAINER_NAME`: Name of container which can signal plymouth to quit.
 - `THEME_NAME`: Name of the plymouth theme to activate.
 - `THEME_DIR`: Directory in which themes are stored.

## Usage:
1. Create a folder containing your plymouth themes inside this folder such as `./themes`
2. Build the docker image: `docker build . -f Dockerfile.ARCH --build-arg THEME_DIR=themes --build-arg CONTAINER_NAME=mycroft --build-arg THEME_NAME=mycroft -t pv_plymouth.ARCH`
3. Extract the addon from the container image: `docker cp pv_plymouth.ARCH:/out/addon-plymouth.cpio.xz4 addon-plymouth.cpio.xz4`

